package com.bettercoding.hex.domain.restaurant.port.secondary;

import com.bettercoding.hex.domain.restaurant.port.shared.OrderDetailsDto;

public interface OrderDetails {
    OrderDetailsDto getOrderDetails(int orderId);
}
