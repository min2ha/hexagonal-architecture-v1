package com.bettercoding.hex.test.domain.restaurant;

import com.bettercoding.hex.domain.restaurant.RestaurantFacade;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderNotification;
import com.bettercoding.hex.domain.restaurant.port.shared.OrderDetailsDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

class RestaurantDomainTest {

    @Test
    public void prepareOrderTest() {
        //TODO implement mock using mockito instead of the following section
        OrderDetails orderDetails = new OrderDetails() {
            @Override
            public OrderDetailsDto getOrderDetails(int orderId) {
                return new OrderDetailsDto(orderId, "Pizza");
            }
        };

        AtomicBoolean notificationWasSent = new AtomicBoolean(false);

        OrderNotification orderNotification = orderId -> notificationWasSent.set(true);

        RestaurantFacade restaurantFacade = new RestaurantFacade(orderDetails, orderNotification);

        //When
        restaurantFacade.getCookCommandService().prepareOrder(1);

        //Then markAsReadyToDelivery notification was sent
        Assertions.assertTrue(notificationWasSent.get());

    }
}
