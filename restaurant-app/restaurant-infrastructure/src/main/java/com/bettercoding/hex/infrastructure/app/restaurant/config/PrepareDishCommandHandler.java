package com.bettercoding.hex.infrastructure.app.restaurant.config;

import com.bettercoding.hex.domain.restaurant.port.primary.CookCommandService;
import com.bettercoding.hex.infrastructure.CommandHandler;
import com.bettercoding.hex.infrastructure.command.PrepareDishCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class PrepareDishCommandHandler implements CommandHandler<PrepareDishCommand> {
    private final CookCommandService cookCommandService;

    @Override
    public void handle(PrepareDishCommand command) {
        cookCommandService.prepareOrder(command.getOrderId());
    }

    @Override
    public Class<?> getHandledCommand() {
        return PrepareDishCommand.class;
    }
}
