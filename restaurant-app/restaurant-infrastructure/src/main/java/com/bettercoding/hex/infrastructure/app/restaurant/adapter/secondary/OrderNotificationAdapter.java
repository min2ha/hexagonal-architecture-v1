package com.bettercoding.hex.infrastructure.app.restaurant.adapter.secondary;

import com.bettercoding.hex.domain.restaurant.port.secondary.OrderNotification;
import com.bettercoding.hex.infrastructure.CommandBus;
import com.bettercoding.hex.infrastructure.command.MarkOrderAsReadyToDeliveryCommand;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class OrderNotificationAdapter implements OrderNotification {
    private final CommandBus commandBus;

    @Override
    public void orderReady(int orderId) {
        commandBus.fire(new MarkOrderAsReadyToDeliveryCommand(orderId));
    }
}
