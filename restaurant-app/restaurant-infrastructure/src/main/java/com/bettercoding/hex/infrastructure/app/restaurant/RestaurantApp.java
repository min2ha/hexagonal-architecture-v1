package com.bettercoding.hex.infrastructure.app.restaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(scanBasePackages = "com.bettercoding.hex")
public class RestaurantApp {
    public static void main(String[] args) {
        SpringApplication.run(RestaurantApp.class, args);
    }

    @RestController
    public static class HelloRestController {
        @GetMapping("/appName")
        public ResponseEntity<String> getAppName() {
            return ResponseEntity.ok("RestaurantApp");
        }
    }
}
