package com.bettercoding.hex.infrastructure.app.restaurant.adapter.primary;


import com.bettercoding.hex.domain.restaurant.RestaurantFacade;
import com.bettercoding.hex.domain.restaurant.port.primary.CookCommandService;
import lombok.experimental.Delegate;
import org.springframework.stereotype.Service;

@Service
class CookCommandAppService implements CookCommandService {
    @Delegate
    private final CookCommandService delegate;

    public CookCommandAppService(RestaurantFacade restaurantFacade) {
        delegate = restaurantFacade.getCookCommandService();
    }
}
