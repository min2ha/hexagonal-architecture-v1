package com.bettercoding.hex.infrastructure.app.restaurant.adapter.secondary;

import com.bettercoding.hex.domain.restaurant.RestaurantFacade;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderNotification;
import com.bettercoding.hex.infrastructure.CommandBus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration("config-restaurant-domain")
class Config {
    @Bean
    RestaurantFacade restaurantFacade(OrderDetails orderDetails, OrderNotification orderNotification) {
        return new RestaurantFacade(orderDetails, orderNotification);
    }

    @Bean("restaurant-order-details")
    OrderDetails orderDetails(@Qualifier("food-order-service-restaurant-rest-template") RestTemplate restTemplate, @Value("${foodOrderEndpointUrl}") String foodOrderEndpointUrl) {
        return new FoodOrderDetailsRestAdapter(restTemplate, foodOrderEndpointUrl);
    }

    @Bean
    OrderNotification restaurantOrderNotification(CommandBus commandBus) {
        return new OrderNotificationAdapter(commandBus);
    }

    @Bean("food-order-service-restaurant-rest-template")
    RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
