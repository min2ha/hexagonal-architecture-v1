package com.bettercoding.hex.infrastructure;

import com.bettercoding.hex.infrastructure.command.Command;

public interface CommandHandler<T extends Command> {
    void handle(T command);

    Class<?> getHandledCommand();
}
