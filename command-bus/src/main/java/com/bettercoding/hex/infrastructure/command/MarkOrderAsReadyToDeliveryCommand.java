package com.bettercoding.hex.infrastructure.command;

import com.bettercoding.hex.infrastructure.command.Command;
import lombok.Value;

@Value
public class MarkOrderAsReadyToDeliveryCommand implements Command {
    int orderId;

}
