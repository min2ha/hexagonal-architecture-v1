package com.bettercoding.hex.infrastructure.command;

import com.bettercoding.hex.infrastructure.command.Command;
import lombok.Value;

@Value
public class PrepareDishCommand implements Command {
    int orderId;

}
