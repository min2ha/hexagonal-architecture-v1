package com.bettercoding.test;


import com.bettercoding.hex.FoodOrderApp;
import com.bettercoding.hex.domain.order.port.primary.FoodOrderCommandService;
import com.bettercoding.hex.domain.order.port.primary.FoodOrderQueryService;
import com.bettercoding.hex.domain.order.port.shared.OrderState;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest(classes = FoodOrderApp.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CrossDomainSysTest {

    @Autowired
    private FoodOrderCommandService foodOrderCommandService;
    @Autowired
    private FoodOrderQueryService foodOrderQueryService;

    @Test
    public void basicScenario() {
        //Given

        //When
        int orderId = foodOrderCommandService.createOrder("Pizza", "Wall Street");

        //then
        Assertions.assertEquals(OrderState.NEW, foodOrderQueryService.getOrderDetails(orderId).getOrderState());

        //when
        foodOrderCommandService.makeOrders();

        //then
        Assertions.assertEquals(OrderState.READY_TO_DELIVERY, foodOrderQueryService.getOrderDetails(orderId).getOrderState());

        //when
        foodOrderCommandService.makeOrders();

        //then
        Assertions.assertEquals(OrderState.DELIVERED, foodOrderQueryService.getOrderDetails(orderId).getOrderState());
    }
}
