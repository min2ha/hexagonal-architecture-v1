package com.bettercoding.hex.infrastructure.app.order.api;

public interface OrderDetailsClient {
    OrderDetailsResponse getOrderDetails(int orderId);
}
