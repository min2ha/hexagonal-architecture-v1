package com.bettercoding.hex.domain.order;

import com.bettercoding.hex.domain.order.port.primary.FoodOrderCommandService;
import com.bettercoding.hex.domain.order.port.primary.FoodOrderQueryService;
import com.bettercoding.hex.domain.order.port.secondary.Logistics;
import com.bettercoding.hex.domain.order.port.secondary.OrderStore;
import lombok.Getter;


@Getter
public class OrderFacade {
    private final FoodOrderCommandService foodOrderCommandService;
    private final FoodOrderQueryService foodOrderQueryService;

    public OrderFacade(OrderStore orderStore, Logistics logistics) {
        foodOrderCommandService = new FoodOrderCommandServiceImpl(orderStore, logistics);
        foodOrderQueryService = new FoodOrderQueryFacadeImpl(orderStore);
    }
}
