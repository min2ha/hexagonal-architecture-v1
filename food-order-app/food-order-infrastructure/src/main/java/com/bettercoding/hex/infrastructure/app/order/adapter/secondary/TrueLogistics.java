package com.bettercoding.hex.infrastructure.app.order.adapter.secondary;

import com.bettercoding.hex.domain.order.port.secondary.Logistics;
import com.bettercoding.hex.infrastructure.CommandBus;
import com.bettercoding.hex.infrastructure.command.DeliverOrderCommand;
import com.bettercoding.hex.infrastructure.command.PrepareDishCommand;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class TrueLogistics implements Logistics {
    private final CommandBus commandBus;

    @Override
    public void prepareOrder(int orderId) {
        commandBus.fire(new PrepareDishCommand(orderId));
    }

    @Override
    public void deliver(int orderId) {
        commandBus.fire((new DeliverOrderCommand(orderId)));
    }
}
