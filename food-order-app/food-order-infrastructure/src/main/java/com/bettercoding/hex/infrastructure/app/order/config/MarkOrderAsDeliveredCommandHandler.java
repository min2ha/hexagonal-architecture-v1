package com.bettercoding.hex.infrastructure.app.order.config;

import com.bettercoding.hex.domain.order.port.primary.FoodOrderCommandService;
import com.bettercoding.hex.infrastructure.CommandHandler;
import com.bettercoding.hex.infrastructure.command.MarkOrderAsDeliveredCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class MarkOrderAsDeliveredCommandHandler implements CommandHandler<MarkOrderAsDeliveredCommand> {
    private final FoodOrderCommandService foodOrderCommandService;

    @Override
    public void handle(MarkOrderAsDeliveredCommand command) {
        foodOrderCommandService.markAsDelivered(command.getOrderId());
    }

    @Override
    public Class<?> getHandledCommand() {
        return MarkOrderAsDeliveredCommand.class;
    }
}
