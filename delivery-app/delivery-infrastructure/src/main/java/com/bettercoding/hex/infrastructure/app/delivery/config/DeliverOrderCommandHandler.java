package com.bettercoding.hex.infrastructure.app.delivery.config;

import com.bettercoding.hex.domain.delivery.port.primary.DeliveryCommandService;
import com.bettercoding.hex.infrastructure.CommandHandler;
import com.bettercoding.hex.infrastructure.command.DeliverOrderCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class DeliverOrderCommandHandler implements CommandHandler<DeliverOrderCommand> {
    private final DeliveryCommandService deliveryCommandService;

    @Override
    public void handle(DeliverOrderCommand command) {
        deliveryCommandService.deliverOrder(command.getOrderId());
    }

    @Override
    public Class<?> getHandledCommand() {
        return DeliverOrderCommand.class;
    }
}
