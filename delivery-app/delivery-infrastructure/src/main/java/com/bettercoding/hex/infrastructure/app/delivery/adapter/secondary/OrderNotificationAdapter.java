package com.bettercoding.hex.infrastructure.app.delivery.adapter.secondary;

import com.bettercoding.hex.domain.delivery.port.secondary.OrderNotification;
import com.bettercoding.hex.infrastructure.CommandBus;
import com.bettercoding.hex.infrastructure.command.MarkOrderAsDeliveredCommand;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class OrderNotificationAdapter implements OrderNotification {
    private final CommandBus commandBus;

    @Override
    public void orderDelivered(int orderId) {
        commandBus.fire(new MarkOrderAsDeliveredCommand(orderId));
    }
}
