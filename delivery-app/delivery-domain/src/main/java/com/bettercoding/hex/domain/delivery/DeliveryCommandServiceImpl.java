package com.bettercoding.hex.domain.delivery;

import com.bettercoding.hex.domain.delivery.port.primary.DeliveryCommandService;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderNotification;
import com.bettercoding.hex.domain.delivery.port.shared.OrderDetailsDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class DeliveryCommandServiceImpl implements DeliveryCommandService {
    private final OrderDetails orderDetails;
    private final OrderNotification orderNotification;

    @Override
    public void deliverOrder(int orderId) {
        OrderDetailsDto details = orderDetails.getOrderDetails(orderId);
        log.info("Delivering orderId {} to {}", details.getOrderId(), details.getAddress());
        orderNotification.orderDelivered(details.getOrderId());
    }
}
