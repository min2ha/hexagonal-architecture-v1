package com.bettercoding.hex.domain.delivery.port.primary;

public interface DeliveryCommandService {
    void deliverOrder(int orderId);
}
