package com.bettercoding.hex.domain.delivery.port.secondary;

public interface OrderNotification {
    void orderDelivered(int orderId);
}
