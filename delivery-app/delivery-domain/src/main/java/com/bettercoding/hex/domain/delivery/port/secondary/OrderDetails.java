package com.bettercoding.hex.domain.delivery.port.secondary;

import com.bettercoding.hex.domain.delivery.port.shared.OrderDetailsDto;

public interface OrderDetails {
    OrderDetailsDto getOrderDetails(int orderId);
}
